package facci.korepin.convertidorlibras;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class Convertir extends AppCompatActivity {

    TextView txtingrese;

    int valor;
    double resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertir);

        txtingrese = (TextView) findViewById(R.id.txtingrese);
        //Bundle bundle= new Bundle();
        Bundle bundle = this.getIntent().getExtras();
        txtingrese.setText(bundle.getString("dato"));

        valor= Integer.parseInt(bundle.getString("dato"));
        resultado = valor * 453.592;

        txtingrese.setText(String.valueOf(resultado));




        Log.e("Valor", "Conversión Finalizada");
    }
}
